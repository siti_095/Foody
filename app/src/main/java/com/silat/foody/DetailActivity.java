package com.silat.foody;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;

public class DetailActivity extends AppCompatActivity {
    ImageView imgLogo;
    TextView textNama, textTelepon, textAlamat, textDeskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setTitle("Detail Kampus");

        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        textNama = (TextView) findViewById(R.id.textNama);
        textTelepon = (TextView) findViewById(R.id.textTelepon);
        textAlamat = (TextView) findViewById(R.id.textAlamat);
        textDeskripsi = (TextView) findViewById(R.id.textDeskripsi);

        Intent intent = getIntent();
        HashMap<String, String> hashMap = (HashMap<String, String>)
                intent.getSerializableExtra("map");
        Glide.with(this).load(hashMap.get("logo")).into(imgLogo);

        textNama.setText(hashMap.get("nama"));
        textTelepon.setText(hashMap.get("telepon"));
        SpannableString content = new SpannableString(hashMap.get("alamat"));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textAlamat.setText(content);
        textDeskripsi.setText(hashMap.get("deskripsi"));
    }

    public void openMap(View v) {
        String map = "http://maps.google.co.in/maps?q=" + textAlamat.getText().toString().trim();

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
        startActivity(i);
    }
}

