package com.silat.foody;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

public class Adapter extends
        RecyclerView.Adapter<Adapter.ViewHolder> {

    private ArrayList<HashMap<String, String>> postList;
    private MainActivity activity;

    public Adapter(ArrayList<HashMap<String, String>> postList,
                           MainActivity mainActivity) {
        this.postList = postList;
        this.activity = mainActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.activity_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final HashMap<String, String> post = postList.get(position);

        Glide.with(activity).load(post.get("logo")).into(viewHolder.imgLogo);
        viewHolder.textNama.setText(post.get("nama"));
        viewHolder.textTelepon.setText(post.get("telepon"));
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgLogo;
        TextView textNama;
        TextView textTelepon;

        public ViewHolder(View view) {
            super(view);
            imgLogo = (ImageView) view.findViewById(R.id.imgLogo);
            textTelepon = (TextView) view.findViewById(R.id.textTelepon);
            textNama = (TextView) view.findViewById(R.id.textNama);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            final HashMap<String, String> post = postList.get(getAdapterPosition());

            Intent intent = new Intent(activity, DetailActivity.class);
            intent.putExtra("map", post);
            activity.startActivity(intent);
        }
    }

}

